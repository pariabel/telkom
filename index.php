<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Telkom Indonesia</title>
<meta name="description" content=""/>
<meta name="keywords" content=""/>
<link rel="icon" href="./assets/images/favicon.ico" type="image/gif">

<!-- Bootstrap -->
<link rel="stylesheet" href="./assets/lib/bootstrap/css/bootstrap.css">

<!-- Fontawesome -->
<link rel="stylesheet" href="./assets/lib/fontawesome/css/all.css">

<!-- arcGIS-->
<link rel="stylesheet" href="https://js.arcgis.com/4.18/esri/themes/dark/main.css" />

<!-- Master -->
<link rel="stylesheet" href="./assets/css/master.css">

<style>
  html, body, #viewDiv {
    height: 100%;
    width: 100%;
    margin: 0;
    padding: 0;
  }
  #infoDiv {
    padding: 10px;
    width: 275px;
  }
  #sliderValue{
    font-weight: bolder;
  }
  #legendDiv{
    width: 260px;
  }
  #description{
    padding: 10px 0 10px 0;
  }
</style>

</head>
<body class="bg-black">
    <div class="box-wrapper">
            <div class="row">
                    <div class="col-xs-12 col-sm-12 col-lg-2">
                            <div class="box-content">
                                    <h3>
                                        <a href="#"><i class="fas fa-arrow-left"></i></a> List Kelurahan
                                    </h3>
                                    <ul class="ul-list">
                                            <li><a class="active" href="#">KELAPA GADING BARAT</a></li>
                                            <li><a href="#">KELAPA GADING TIMUR</a></li>
                                            <li><a href="#">SUNTER JAYA</a></li>
                                    </ul>
                            </div>
                            <div class="box-content">
                                    <h3>
                                            <a href="#"><i class="fas fa-arrow-left"></i></a> Info
                                    </h3>
                                    <div class="padding-default">
                                            <div><b>Region Profil DKI: 25458</b></div>
                                            <div class="box-info">
                                                    <div class="left-info">
                                                            Kelurahan
                                                    </div>
                                                    <div class="right-info">
                                                            KELAPA GADING BARAT
                                                    </div>
                                                    <div class="clearer"></div>
                                            </div>
                                            <div class="box-info">
                                                    <div class="left-info">
                                                            Jumlah Penduduk
                                                    </div>
                                                    <div class="right-info">
                                                            10000
                                                    </div>
                                                    <div class="clearer"></div>
                                            </div>
                                            <div class="box-info">
                                                    <div class="left-info">
                                                            Jumlah KK
                                                    </div>
                                                    <div class="right-info">
                                                            1000
                                                    </div>
                                                    <div class="clearer"></div>
                                            </div>
                                            <div class="box-info">
                                                    <div class="left-info">
                                                            Call Interest
                                                    </div>
                                                    <div class="right-info">
                                                            Trading & Distribution Service, helatcare & Walfare Service
                                                    </div>
                                                    <div class="clearer"></div>
                                            </div>
                                    </div>
                            </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-10">
                            <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-lg-6">
                                            <div class="box-content">
                                                    <div id="viewDiv"></div>
                                                    <div id="infoDiv" class="esri-widget">
                                                        <div id="description">
                                                           <span id="sliderValue">0</span> megawatts of capacity
                                                        </div>
                                                        <div id="sliderContainer">
                                                          <div id="sliderDiv"></div>
                                                        </div>
                                                        <div id="legendDiv"></div>
                                                    </div>
                                            </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-lg-2">
                                            <div class="box-content">
                                                    <h3>
                                                            <a href="#"><i class="fas fa-arrow-left"></i></a>
                                                            Top 10 Web Interest
                                                    </h3>
                                                    <ul class="ul-list-alt">
                                                            <li>SocialNet</li>
                                                            <li>P2P</li>
                                                            <li>Enterprise</li>
                                                            <li>Video</li>
                                                            <li>Mail</li>
                                                            <li>GoogleServices</li>
                                                            <li>Games</li>
                                                            <li>CDN</li>
                                                            <li>Technology</li>
                                                            <li>CloudStorage</li>
                                                    </ul>
                                            </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-lg-2">
                                            <div class="box-content">
                                                    <h3>
                                                            <a href="#"><i class="fas fa-arrow-left"></i></a> Top 10 Call Interest
                                                    </h3>
                                                    <ul class="ul-list-alt">
                                                            <li>TRAIDING AND DISTRIBUTION SERVICE</li>
                                                            <li>HEALTCARE AND WELFARE SERVICE </li>
                                                            <li>TRAIDING AND DISTRIBUTION SERVICE</li>
                                                            <li>HEALTCARE AND WELFARE SERVICE </li>
                                                            <li>TRAIDING AND DISTRIBUTION SERVICE</li>
                                                            <li>HEALTCARE AND WELFARE SERVICE </li>
                                                            <li>TRAIDING AND DISTRIBUTION SERVICE</li>
                                                            <li>HEALTCARE AND WELFARE SERVICE </li>
                                                            <li>TRAIDING AND DISTRIBUTION SERVICE</li>
                                                            <li>HEALTCARE AND WELFARE SERVICE </li>
                                                    </ul>
                                            </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-lg-2">
                                            <div class="box-content">
                                                    <h3>
                                                            <a href="#"><i class="fas fa-arrow-left"></i></a> Top 10 TV Program
                                                    </h3>
                                                    <ul class="ul-list-alt">
                                                            <li>GENERAL & ENTERTAINMENT</li>
                                                            <li>NEWS </li>
                                                            <li>TV SERIES</li>
                                                            <li>NEWS </li>
                                                            <li>TV SERIES</li>
                                                            <li>NEWS </li>
                                                            <li>TV SERIES</li>
                                                            <li>NEWS </li>
                                                            <li>TV SERIES</li>
                                                            <li>NEWS </li>
                                                    </ul>
                                            </div>
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-lg-4">
                                            <div class="box-content">
                                                    <h3>
                                                            <a href="#"><i class="fas fa-arrow-left"></i></a>
                                                            ARPU (%)
                                                    </h3>
                                                    <div class="tab-content">

                                                            <div id="arpu" class="tab-pane fade in active">

                                                                    <div id="bar-chart">
                                                                            <div class="graph">
                                                                                    <ul class="x-axis">
                                                                                            <li><span><100</span></li>
                                                                                            <li><span>100-500</span></li>
                                                                                            <li><span>500-1000</span></li>
                                                                                            <li><span>1000-3000</span></li>
                                                                                            <li><span>>3000</span></li>
                                                                                    </ul>
                                                                                    <ul class="y-axis">
                                                                                            <li><span>80</span></li>
                                                                                            <li><span>60</span></li>
                                                                                            <li><span>40</span></li>
                                                                                            <li><span>20</span></li>
                                                                                            <li><span>0</span></li>
                                                                                    </ul>
                                                                                    <div class="bars">
                                                                                            <div class="bar-group">
                                                                                                    <div class="bar bar-1 stat-1" style="height: 10%;"></div>
                                                                                            </div>

                                                                                            <div class="bar-group">
                                                                                                    <div class="bar bar-1 stat-1" style="height: 20%;"></div>
                                                                                            </div>

                                                                                            <div class="bar-group">
                                                                                                    <div class="bar bar-1 stat-1" style="height: 80%;"></div>
                                                                                            </div>

                                                                                            <div class="bar-group">
                                                                                                    <div class="bar bar-1 stat-1" style="height: 60%;"></div>
                                                                                            </div>

                                                                                            <div class="bar-group">
                                                                                                    <div class="bar bar-1 stat-1" style="height: 30%;"></div>
                                                                                            </div>

                                                                                    </div>
                                                                            </div>
                                                                    </div>

                                                            </div>

                                                            <div id="ses" class="tab-pane fade">

                                                                    <div id="bar-chart">
                                                                            <div class="graph">
                                                                                    <ul class="x-axis">
                                                                                            <li><span><100</span></li>
                                                                                            <li><span>100-500</span></li>
                                                                                            <li><span>500-1000</span></li>
                                                                                            <li><span>1000-3000</span></li>
                                                                                            <li><span>>3000</span></li>
                                                                                    </ul>
                                                                                    <ul class="y-axis">
                                                                                            <li><span>80</span></li>
                                                                                            <li><span>60</span></li>
                                                                                            <li><span>40</span></li>
                                                                                            <li><span>20</span></li>
                                                                                            <li><span>0</span></li>
                                                                                    </ul>
                                                                                    <div class="bars">
                                                                                            <div class="bar-group">
                                                                                                    <div class="bar bar-1 stat-1" style="height: 10%;"></div>
                                                                                            </div>

                                                                                            <div class="bar-group">
                                                                                                    <div class="bar bar-1 stat-1" style="height: 20%;"></div>
                                                                                            </div>

                                                                                            <div class="bar-group">
                                                                                                    <div class="bar bar-1 stat-1" style="height: 80%;"></div>
                                                                                            </div>

                                                                                            <div class="bar-group">
                                                                                                    <div class="bar bar-1 stat-1" style="height: 60%;"></div>
                                                                                            </div>

                                                                                            <div class="bar-group">
                                                                                                    <div class="bar bar-1 stat-1" style="height: 30%;"></div>
                                                                                            </div>

                                                                                    </div>
                                                                            </div>
                                                                    </div>

                                                            </div>

                                                    </div>
                                            </div>
                                            <ul class="nav nav-pills">
                                                    <li class="active"><a data-toggle="pill" href="#arpu">ARPU</a></li>
                                                    <li><a data-toggle="pill" href="#ses">SES</a></li>
                                            </ul>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-lg-3">
                                            <div class="box-content">
                                                    <h3>
                                                            <a href="#"><i class="fas fa-arrow-left"></i></a>
                                                            Age Group (%)
                                                    </h3>
                                                    <div class="tab-content">

                                                            <div id="age" class="tab-pane fade in active">

                                                                    <div id="bar-chart">
                                                                            <div class="graph">
                                                                                    <ul class="x-axis">
                                                                                            <li><span>20</span></li>
                                                                                            <li><span>20-29</span></li>
                                                                                            <li><span>30-44</span></li>
                                                                                            <li><span>45-59</span></li>
                                                                                            <li><span>>60</span></li>
                                                                                    </ul>
                                                                                    <ul class="y-axis">
                                                                                            <li><span>80</span></li>
                                                                                            <li><span>60</span></li>
                                                                                            <li><span>40</span></li>
                                                                                            <li><span>20</span></li>
                                                                                            <li><span>0</span></li>
                                                                                    </ul>
                                                                                    <div class="bars">
                                                                                            <div class="bar-group">
                                                                                                    <div class="bar bar-1 stat-2" style="height: 10%;"></div>
                                                                                            </div>

                                                                                            <div class="bar-group">
                                                                                                    <div class="bar bar-1 stat-2" style="height: 20%;"></div>
                                                                                            </div>

                                                                                            <div class="bar-group">
                                                                                                    <div class="bar bar-1 stat-2" style="height: 80%;"></div>
                                                                                            </div>

                                                                                            <div class="bar-group">
                                                                                                    <div class="bar bar-1 stat-2" style="height: 60%;"></div>
                                                                                            </div>

                                                                                            <div class="bar-group">
                                                                                                    <div class="bar bar-1 stat-2" style="height: 30%;"></div>
                                                                                            </div>

                                                                                    </div>
                                                                            </div>
                                                                    </div>

                                                            </div>

                                                            <div id="gender" class="tab-pane fade">

                                                                    <div id="bar-chart">
                                                                            <div class="graph">
                                                                                    <ul class="x-axis">
                                                                                            <li><span>Pria</span></li>
                                                                                            <li><span>Wanita</span></li>
                                                                                    </ul>
                                                                                    <ul class="y-axis">
                                                                                            <li><span>80</span></li>
                                                                                            <li><span>60</span></li>
                                                                                            <li><span>40</span></li>
                                                                                            <li><span>20</span></li>
                                                                                            <li><span>0</span></li>
                                                                                    </ul>
                                                                                    <div class="bars">
                                                                                            <div class="bar-group">
                                                                                                    <div class="bar bar-1 stat-2" style="height: 10%;"></div>
                                                                                            </div>

                                                                                            <div class="bar-group">
                                                                                                    <div class="bar bar-1 stat-2" style="height: 20%;"></div>
                                                                                            </div>
                                                                                    </div>
                                                                            </div>
                                                                    </div>

                                                            </div>

                                                    </div>
                                            </div>
                                            <ul class="nav nav-pills">
                                                    <li class="active"><a data-toggle="pill" href="#age">Age</a></li>
                                                    <li><a data-toggle="pill" href="#gender">Gender</a></li>
                                            </ul>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-lg-3">
                                            <div class="box-content">
                                                    <h3>
                                                            <a href="#"><i class="fas fa-arrow-left"></i></a> Public Buildings
                                                    </h3>

                                                    <div id="bar-chart">
                                                            <div class="graph">
                                                                    <ul class="y-axis">
                                                                            <li><span>80</span></li>
                                                                            <li><span>60</span></li>
                                                                            <li><span>40</span></li>
                                                                            <li><span>20</span></li>
                                                                            <li><span>0</span></li>
                                                                    </ul>
                                                                    <div class="bars">
                                                                            <div class="bar-group">
                                                                                    <div class="bar bar-1 stat-3" style="height: 10%;"></div>
                                                                            </div>
                                                                            <div class="bar-group">
                                                                                    <div class="bar bar-1 stat-3" style="height: 20%;"></div>
                                                                            </div>
                                                                            <div class="bar-group">
                                                                                    <div class="bar bar-1 stat-3" style="height: 80%;"></div>
                                                                            </div>
                                                                    </div>
                                                            </div>
                                                            <div class="padding-default text-center"><small>Resident Shopping Mall Office</small></div>
                                                    </div>


                                            </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-lg-2">
                                            <div class="box-content">
                                                    <h3>
                                                            <a href="#"><i class="fas fa-arrow-left"></i></a>Everage Telco Risk
                                                    </h3>

                                                    <div class="box-table">
                                                            <div class="display-table">
                                                                    <div class="display-table-cell">
                                                                            <div class="box-value">
                                                                                    <h1>54.80</h1>
                                                                            </div>
                                                                    </div>
                                                            </div>
                                                    </div>
                                            </div>
                                    </div>
                            </div>
                    </div>
            </div>

    </div>


<!-- Jquery -->
<script type="text/javascript" src="./assets/js/jquery.min.js"></script>

<!-- Bootsrap -->
<script type="text/javascript" src="./assets/lib/bootstrap/js/bootstrap.js"></script>
<script src="https://js.arcgis.com/4.18/"></script>
<script>
  require([
    "esri/Map",
    "esri/views/MapView",
    "esri/layers/FeatureLayer",
    "esri/widgets/Legend",
    "esri/widgets/Slider",
    "esri/widgets/Expand"
  ], function(Map, MapView, FeatureLayer, Legend, Slider, Expand
  ) {
    // Configure clustering on the layer with a
    // popupTemplate displaying the predominant
    // fuel type of the power plants in the cluster

    const clusterLabelThreshold = 1500;

    const haloColor = "#373837";
    const color = "#f0f0f0";

    const clusterConfig = {
      type: "cluster",
      popupTemplate: {
        content: [
          {
            type: "text",
            text: "This cluster represents <b>{cluster_count}</b> power plants with an average capacity of <b>{cluster_avg_capacity_mw} megawatts</b>."
             + " The power plants in this cluster produce a total of <b>{expression/total-mw} megawatts</b> of power."
          },
          {
            type: "text",
            text: "Most power plants in this cluster generate power from <b>{cluster_type_fuel1}</b>."
          }
        ],
        fieldInfos: [{
          fieldName: "cluster_count",
          format: {
            places: 0,
            digitSeparator: true
          }
        }, {
          fieldName: "cluster_avg_capacity_mw",
          format: {
            places: 2,
            digitSeparator: true
          }
        }, {
          fieldName: "expression/total-mw",
          format: {
            places: 0,
            digitSeparator: true
          }
        }],
        expressionInfos: [{
          name: "total-mw",
          title: "total megawatts",
          expression: "$feature.cluster_avg_capacity_mw * $feature.cluster_count"
        }]
      },
      // larger radii look better with multiple label classes
      // smaller radii looks better visually
      clusterRadius: "120px",
      labelsVisible: true,
      labelingInfo: [{
        symbol: {
          type: "text",
          haloColor,
          haloSize: "1px",
          color,
          font: {
            family: "Noto Sans",
            size: "11px"
          },
          xoffset: 0,
          yoffset: "-15px",
        },
        labelPlacement: "center-center",
        labelExpressionInfo: {
          expression: "Text($feature.cluster_count, '#,### plants')"
        },
        where: `cluster_avg_capacity_mw > ${clusterLabelThreshold}`
      }, {
        symbol: {
          type: "text",
          haloColor,
          haloSize: "2px",
          color,
          font: {
            weight: "bold",
            family: "Noto Sans",
            size: "18px"
          },
          xoffset: 0,
          yoffset: 0
        },
        labelPlacement: "center-center",
        labelExpressionInfo: {
          expression: "$feature.cluster_type_fuel1"
        },
        where: `cluster_avg_capacity_mw > ${clusterLabelThreshold}`
      }, {
        symbol: {
          type: "text",
          haloColor,
          haloSize: "1px",
          color,
          font: {
            weight: "bold",
            family: "Noto Sans",
            size: "12px"
          },
          xoffset: 0,
          yoffset: "15px"
        },
        deconflictionStrategy: "none",
        labelPlacement: "center-center",
        labelExpressionInfo: {
          expression: `
          var value = $feature.cluster_avg_capacity_mw;
          var num = Count(Text(Round(value)));

          Decode(num,
            4, Text(value / Pow(10, 3), "##.0k"),
            5, Text(value / Pow(10, 3), "##k"),
            6, Text(value / Pow(10, 3), "##k"),
            7, Text(value / Pow(10, 6), "##.0m"),
            Text(value, "#,###")
          );
          `
        },
        where: `cluster_avg_capacity_mw > ${clusterLabelThreshold}`
      }, {
        symbol: {
          type: "text",
          haloColor,
          haloSize: "1px",
          color,
          font: {
            family: "Noto Sans",
            size: "11px"
          },
          xoffset: 0,
          yoffset: "-15px",
        },
        labelPlacement: "above-right",
        labelExpressionInfo: {
          expression: "Text($feature.cluster_count, '#,### plants')"
        },
        where: `cluster_avg_capacity_mw <= ${clusterLabelThreshold}`
      }, {
        symbol: {
          type: "text",
          haloColor,
          haloSize: "2px",
          color,
          font: {
            weight: "bold",
            family: "Noto Sans",
            size: "18px"
          }
        },
        labelPlacement: "above-right",
        labelExpressionInfo: {
          expression: "$feature.cluster_type_fuel1"
        },
        where: `cluster_avg_capacity_mw <= ${clusterLabelThreshold}`
      },  {
        symbol: {
          type: "text",
          haloColor,
          haloSize: "1px",
          color,
          font: {
            weight: "bold",
            family: "Noto Sans",
            size: "12px"
          },
          xoffset: 0,
          yoffset: 0
        },
        labelPlacement: "center-center",
        labelExpressionInfo: {
          expression: `
          var value = $feature.cluster_avg_capacity_mw;
          var num = Count(Text(Round(value)));

          Decode(num,
            4, Text(value / Pow(10, 3), "##.0k"),
            5, Text(value / Pow(10, 3), "##k"),
            6, Text(value / Pow(10, 3), "##k"),
            7, Text(value / Pow(10, 6), "##.0m"),
            Text(value, "#,###")
          );
          `
        },
        where: `cluster_avg_capacity_mw <= ${clusterLabelThreshold}`
      }]
    };

    const layer = new FeatureLayer({
      portalItem: {
        id: "eb54b44c65b846cca12914b87b315169"
      },
      featureReduction: clusterConfig,
      popupEnabled: true,
      labelsVisible: true,
      labelingInfo: [{
        symbol: {
          type: "text",
          haloColor,
          haloSize: "1px",
          color,
          font: {
            family: "Noto Sans",
            size: "11px"
          },
          xoffset: 0,
          yoffset: "-15px",
        },
        labelPlacement: "center-center",
        labelExpressionInfo: {
          expression: "$feature.name"
        },
        where: `capacity_mw > ${clusterLabelThreshold}`
      }, {
        symbol: {
          type: "text",
          haloColor,
          haloSize: "2px",
          color,
          font: {
            weight: "bold",
            family: "Noto Sans",
            size: "18px"
          },
          xoffset: 0,
          yoffset: 0
        },
        labelPlacement: "center-center",
        labelExpressionInfo: {
          expression: "$feature.fuel1"
        },
        where: `capacity_mw > ${clusterLabelThreshold}`
      }, {
        symbol: {
          type: "text",
          haloColor,
          haloSize: "1px",
          color,
          font: {
            weight: "bold",
            family: "Noto Sans",
            size: "12px"
          },
          xoffset: 0,
          yoffset: "15px"
        },
        labelPlacement: "center-center",
        labelExpressionInfo: {
          expression: `
          var value = $feature.capacity_mw;
          var num = Count(Text(Round(value)));

          Decode(num,
            4, Text(value / Pow(10, 3), "##.0k"),
            5, Text(value / Pow(10, 3), "##k"),
            6, Text(value / Pow(10, 3), "##k"),
            7, Text(value / Pow(10, 6), "##.0m"),
            Text(value, "#,###")
          );
          `
        },
        where: `capacity_mw > ${clusterLabelThreshold}`
      }, {
        symbol: {
          type: "text",
          haloColor,
          haloSize: "1px",
          color,
          font: {
            family: "Noto Sans",
            size: "11px"
          },
          xoffset: 0,
          yoffset: "-15px",
        },
        labelPlacement: "above-right",
        labelExpressionInfo: {
          expression: "$feature.name"
        },
        where: `capacity_mw <= ${clusterLabelThreshold}`
      }, {
        symbol: {
          type: "text",
          haloColor,
          haloSize: "2px",
          color,
          font: {
            weight: "bold",
            family: "Noto Sans",
            size: "18px"
          }
        },
        labelPlacement: "above-right",
        labelExpressionInfo: {
          expression: "$feature.fuel1"
        },
        where: `capacity_mw <= ${clusterLabelThreshold}`
      }, {
        symbol: {
          type: "text",
          haloColor,
          haloSize: "1px",
          color,
          font: {
            weight: "bold",
            family: "Noto Sans",
            size: "12px"
          },
          xoffset: 0,
          yoffset: 0
        },
        labelPlacement: "center-center",
        labelExpressionInfo: {
          expression: `
          var value = $feature.cluster_avg_capacity_mw;
          var num = Count(Text(Round(value)));

          Decode(num,
            4, Text(value / Pow(10, 3), "##.0k"),
            5, Text(value / Pow(10, 3), "##k"),
            6, Text(value / Pow(10, 3), "##k"),
            7, Text(value / Pow(10, 6), "##.0m"),
            Text(value, "#,###")
          );
          `
        },
        where: `cluster_avg_capacity_mw <= ${clusterLabelThreshold}`
      }]
    });

    const map = new Map({
      basemap: {
        portalItem: {
          id: "8d91bd39e873417ea21673e0fee87604"
        }
      },
      layers: [layer]
    });

    const view = new MapView({
      container: "viewDiv",
      map: map,
      extent: {
        spatialReference: {
          latestWkid: 3857,
          wkid: 102100
        },
        xmin: -42087672,
        ymin: 4108613,
        xmax: -36095009,
        ymax: 8340167
      }
    });

    layer.when().then(function(){
      const renderer = layer.renderer.clone();
      renderer.visualVariables = [{
        type: "size",
        field: "capacity_mw",
        legendOptions: {
          title: "Capacity (MW)"
        },
        minSize: "24px",
        maxSize: "100px",
        minDataValue: 1,
        maxDataValue: 5000
      }];
      layer.renderer = renderer;
    });

    const legend = new Legend({
      view: view,
      container: "legendDiv"
    });

    const infoDiv = document.getElementById("infoDiv");
    view.ui.add(
      new Expand({
        view: view,
        content: infoDiv,
        expandIconClass: "esri-icon-layer-list",
        expanded: false
      }),
      "top-right"
    );

    view.whenLayerView(layer).then(function(layerView) {
      const field = "capacity_mw";

      const slider = new Slider({
        min: 0,
        max: 2000,
        values: [0],
        container: document.getElementById("sliderDiv"),
        visibleElements: {
          rangeLabels: true
        },
        precision: 0
      });

      const sliderValue = document.getElementById("sliderValue");

      // filter features by power plant capacity when the user
      // drags the slider thumb. If clustering is enabled,
      // clusters will recompute and render based on the number
      // and type of features that satisfy the filter where clause

      slider.on(["thumb-change", "thumb-drag"], function(event) {
        sliderValue.innerText = event.value;
        layerView.filter = {
          where: field + " >= " + event.value
        };
      });
    });
  });
</script>
</body>
</html> 